$(document).ready(function() {

  $('html').css('font-size', Math.round(($(window).width() / 2120) * 100 - 2) + '%');

  $('[data-zoom]').click(function() {
    var $this    = $(this);
    var fontSize = Math.floor(parseFloat($('html').css('font-size')));

    if ($this.data('zoom') === 'plus') {
      if (fontSize === 16) { return; }

      fontSize += 1;
    } else {
      if (fontSize === 3 || $('.b-catalog').innerHeight() < $(window).height()) { return; }

      fontSize -= 1;
    }

    $('html').css('font-size', fontSize + 'px');
  });
});